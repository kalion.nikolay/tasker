FROM ubuntu
RUN apt update && apt upgrade -y
RUN apt install -y python3-dev python3-pip git
RUN cd ~ && git clone https://gitlab.com/kalion.nikolay/tasker && python3 -m pip install requirements.txt


